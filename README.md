# QuizApp

## About the Project

### I'm thrilled to share my newly launched Quiz App built with Flutter! It's a dynamic and engaging app designed to challenge your knowledge on various topics, providing a fun way to learn and test your skills.

## Preview

![](./assets/preview/Screencast%20from%202024-02-19%2017-31-01%20(1).gif)

## Acknowledgement

### Massive thanks to Shashi Bagal Sir and Akshay Jagtap Sir for their invaluable support and guidance. None of this would have been possible without their mentorship. 🙏s




