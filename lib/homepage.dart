
import 'package:flutter/material.dart';
import 'package:quiz/quiz2.dart';

class HomePage extends StatefulWidget{
  const HomePage({super.key});

  @override
  State<HomePage> createState()=> _HomePageState();
}

class _HomePageState extends State<HomePage>{


  @override
  Widget build(BuildContext context){
    return Scaffold(
      body:Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  "assets/images/theme.jpg",
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),

          Column(  
            children:[

                const SizedBox(
                  height:100,
                ),

                // const SizedBox(
                //   child: Text(
                //     "Get Ready for",
                //     style: TextStyle(
                      
                //       fontStyle:FontStyle.italic,
                //       fontSize:30,
                //     ),
                //   ),
                // ),

                RichText(
                  textAlign: TextAlign.center,
                  text: const TextSpan(
                    children:[
                      TextSpan(
                        text:"Get Ready for ",
                        style:TextStyle(
                          color:Colors.brown,
                          fontSize: 30,
                          fontStyle: FontStyle.italic,
                        ),
                        
                      ),
                    ],
                  ),
                ),


                RichText(
                  textAlign: TextAlign.center,
                  text: const TextSpan(
                    children:[
                      TextSpan(
                        text:"Quiz ",
                        style:TextStyle(
                          color:Colors.brown,
                          fontSize: 50,
                          fontStyle: FontStyle.italic,
                        ),
                        
                      ),
                    ],
                  ),
                ),

                // const SizedBox(
                  
                //   child: Text(
                //     "Quiz",
                //     style:TextStyle(
                //       fontStyle:FontStyle.italic,
                //       fontSize:50,
                //     ),
                //   ),
                // ),

                // const SizedBox(
                //   height:30,
                // ),

                Image.asset(
                    "assets/images/img3.png",
                     width:650,
                     height:300,
                    
                ),

                const SizedBox(
                  height:80,
                ),
                SizedBox(
                width: 300,
                height: 50,
                child: ElevatedButton(
                  style: ButtonStyle(
                    shape: MaterialStatePropertyAll(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    backgroundColor: MaterialStatePropertyAll(
                      Colors.deepPurple.shade400,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                      MaterialPageRoute(
                        builder: (context) => const QuizApp(),
                      ),
                    );
                  },
                  child: const Text(
                    "Start Quiz",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ),
                ),
            ],

          ),


        ],
      ),
    );
  }

}