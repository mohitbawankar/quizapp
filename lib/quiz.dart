import 'package:flutter/material.dart';

class Quiz extends StatefulWidget {
  const Quiz({super.key});

  @override
  State<Quiz> createState() => _QuizState();
}

class _QuizState extends State<Quiz> {
  int counter = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Quiz App"),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 20,
              ),
              Text(
                "Question $counter/10",
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Question 1: What is flutter?",
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {},
                child: const SizedBox(
                  width: 250,
                  height: 50,
                  child: Text(
                    "Option 1",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {},
                child:  Container(
                  alignment:Alignment.center,
                  width: 250,
                  height: 50,
                  child: const Text(
                    "Option 2",
                    // textAlign: TextAlign.center,
                    
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {},
                child: const SizedBox(
                  width: 250,
                  height: 50,
                  child: Text(
                    "Option 3",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {},
                child: const SizedBox(
                  width: 250,
                  height: 50,
                  child: Text(
                    "Option 4",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState((){
            if(counter < 10){
              counter++;
            }
          });
        },
        child: const Icon(Icons.forward),
      ),
    );
  }
}